<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Class BugsController
 *
 * ==== Задача ====
 *
 * Жуки не любят находиться рядом друг с другом и каждый прячется под отдельным камнем и старается выбирать камни, максимально удаленные от соседей.
 * Так же жуки любят находится максимально далеко от края. Как только жук сел за камень, он более не перемещается. Всего в линии лежат X камней.
 * И туда последовательно бежит прятаться Y жуков. Найти сколько свободных камней будет слева и справа от последнего жука. X может быть до 4 млрд.
 * Написать алгоритм решения задачи.
 *
 * Примеры:
 *
 * X=8, Y=1 – ответ 3,4
 * X=8, Y=2 – ответ 1,2
 * X=8, Y=3 – ответ 1,1
 *
 * ==========
 *
 * @package App\Controller
 */
class BugsController extends AbstractController
{
    /**
     * @Route("/bugs", name="bugs")
     */
    public function index()
    {
        // Кол-во камней (X)
        $rocks_count = 8;

        // Кол-во жуков (Y)
        $bugs_count = 3;

        // Камни это массив, где значения - это зарезервированное жуком место. Если жук прячется под камень, то
        // мы считаем, что камень занят
        for ($i = 0; $i < $rocks_count; $i++) {
            $rocks[] = $i + 1;
        }

        $half = [
            'left' => reset($rocks),
            'right' => end($rocks),
        ];

        // $bugs_count жуков бежит последовательно прятаться в $rocks_count камней
        for ($i = 0; $i < $bugs_count; $i++) {
            // Берем самую большую из половин камней
            $max_half = max($half);

            // Вычисляем размерность левой половины
            $left = ($max_half >> 1) + ($max_half & 1);
            // Вычисляем размерность правой половины
            $right = $max_half - $left--;

            $half['left'] = $left > $half['left'] ? $left : $half['left'];
            $half['right'] = $right < $half['right'] ? $right : $half['right'];
        }

        return $this->render('bugs/index.html.twig', [
            'controller_name' => 'BugsController',
            'rocks_count' => $rocks_count,
            'bugs_count' => $bugs_count,
            'left' => $left,
            'right' => $right,
        ]);
    }
}
