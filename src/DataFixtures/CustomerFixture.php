<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Lot;
use App\Entity\Procedures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CustomerFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $procedures = new Procedures();
            $procedures->setName('procedure ' . $i);

            $lot = new Lot();
            $lot->setProcedures($procedures);
            $lot->setVat(rand(10, 1000));

            $customer = new Customer();
            $customer->setLot($lot);
            $customer->setVat(rand(10, 1000));

            $manager->persist($procedures);
            $manager->persist($lot);
            $manager->persist($customer);
        }

        $manager->flush();
    }
}
