# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.20)
# Database: tek_test
# Generation Time: 2018-10-15 07:27:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lot_id` int(11) NOT NULL,
  `vat` decimal(20,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_81398E09A8CBA5F7` (`lot_id`),
  CONSTRAINT `FK_81398E09A8CBA5F7` FOREIGN KEY (`lot_id`) REFERENCES `lot` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;

INSERT INTO `customer` (`id`, `lot_id`, `vat`)
VALUES
	(21,61,841.00),
	(22,62,300.00),
	(23,63,546.00),
	(24,64,733.00),
	(25,65,658.00),
	(26,66,1000.00),
	(27,67,514.00),
	(28,68,821.00),
	(29,69,753.00),
	(30,70,35.00),
	(31,71,18.00),
	(32,72,651.00),
	(33,73,451.00),
	(34,74,504.00),
	(35,75,628.00),
	(36,76,369.00),
	(37,77,745.00),
	(38,78,880.00),
	(39,79,349.00),
	(40,80,432.00),
	(41,62,300.00),
	(42,62,300.00),
	(43,82,300.00);

/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table lot
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lot`;

CREATE TABLE `lot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procedures_id` int(11) NOT NULL,
  `vat` decimal(20,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B81291B8FBA2A61` (`procedures_id`),
  CONSTRAINT `FK_B81291B8FBA2A61` FOREIGN KEY (`procedures_id`) REFERENCES `procedures` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `lot` WRITE;
/*!40000 ALTER TABLE `lot` DISABLE KEYS */;

INSERT INTO `lot` (`id`, `procedures_id`, `vat`)
VALUES
	(61,221,23.00),
	(62,222,440.00),
	(63,223,388.00),
	(64,224,942.00),
	(65,225,770.00),
	(66,226,871.00),
	(67,227,656.00),
	(68,228,710.00),
	(69,229,780.00),
	(70,230,500.00),
	(71,231,686.00),
	(72,232,427.00),
	(73,233,558.00),
	(74,234,236.00),
	(75,235,489.00),
	(76,236,661.00),
	(77,237,159.00),
	(78,238,641.00),
	(79,239,205.00),
	(80,240,823.00),
	(81,230,500.00),
	(82,222,440.00);

/*!40000 ALTER TABLE `lot` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migration_versions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration_versions`;

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;

INSERT INTO `migration_versions` (`version`)
VALUES
	('20181013063057');

/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table procedures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `procedures`;

CREATE TABLE `procedures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `procedures` WRITE;
/*!40000 ALTER TABLE `procedures` DISABLE KEYS */;

INSERT INTO `procedures` (`id`, `name`)
VALUES
	(221,'procedure 0'),
	(222,'procedure 1'),
	(223,'procedure 2'),
	(224,'procedure 3'),
	(225,'procedure 4'),
	(226,'procedure 5'),
	(227,'procedure 6'),
	(228,'procedure 7'),
	(229,'procedure 8'),
	(230,'procedure 9'),
	(231,'procedure 10'),
	(232,'procedure 11'),
	(233,'procedure 12'),
	(234,'procedure 13'),
	(235,'procedure 14'),
	(236,'procedure 15'),
	(237,'procedure 16'),
	(238,'procedure 17'),
	(239,'procedure 18'),
	(240,'procedure 19');

/*!40000 ALTER TABLE `procedures` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
