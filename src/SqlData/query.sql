SELECT
       id,
       name,
       (SELECT SUM(vat) FROM lot WHERE procedures_id = procedures.id) as l_vat,
       (SELECT SUM(vat) FROM customer WHERE lot_id IN (SELECT id FROM lot WHERE procedures_id = procedures.id)) AS c_vat
FROM procedures; 